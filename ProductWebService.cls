@RestResource(urlMapping='/productWS/*') // E.g: https://intanceurl/services/apexrest/productWS
global with sharing class ProductWebService {

    @HttpPost
    global static String createProduct(String productName, String productCode, String productDescription)
    {
        try
        {
            Product2 objProduct = new Product2();
            objProduct.Name = productName;
            objProduct.ProductCode = productCode;
            objProduct.Description = productDescription;
            
            insert objProduct;    
            
            return 'Prodcut has been created with Id:' + objProduct.Id;
            
        }
        catch(Exception ex)
        {
            return 'Error in creating product:' + ex.getMessage();
        }
    }
}